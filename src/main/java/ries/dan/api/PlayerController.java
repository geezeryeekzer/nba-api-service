package ries.dan.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ries.dan.model.Player;
import ries.dan.service.PlayerService;

import java.util.List;

@RestController
@RequestMapping(path = "/api/players")
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @RequestMapping(path = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Player> getAllPlayers() {
        return playerService.getPlayers();
    }

}
