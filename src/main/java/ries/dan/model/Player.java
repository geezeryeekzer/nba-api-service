package ries.dan.model;

import javax.persistence.*;

@Table
@Entity
public class Player {

    @Id
    private String personId;
    private String firstName;
    private String lastName;
    private String jersey;
    private String pos;
    private String posExpanded;
    private String heightFeet;
    private String heightInches;
    private String weightPounds;
    private TeamData teamData;
    private Boolean isAllStar;
    private char orderChar;
    private String playerUrl;
    private String displayName;

    public Player() { }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getJersey() {
        return jersey;
    }

    public void setJersey(String jersey) {
        this.jersey = jersey;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getPosExpanded() {
        return posExpanded;
    }

    public void setPosExpanded(String posExpanded) {
        this.posExpanded = posExpanded;
    }

    public String getHeightFeet() {
        return heightFeet;
    }

    public void setHeightFeet(String heightFeet) {
        this.heightFeet = heightFeet;
    }

    public String getHeightInches() {
        return heightInches;
    }

    public void setHeightInches(String heightInches) {
        this.heightInches = heightInches;
    }

    public String getWeightPounds() {
        return weightPounds;
    }

    public void setWeightPounds(String weightPounds) {
        this.weightPounds = weightPounds;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public TeamData getTeamData() {
        return teamData;
    }

    public void setTeamData(TeamData teamData) {
        this.teamData = teamData;
    }

    public Boolean getAllStar() {
        return isAllStar;
    }

    public void setAllStar(Boolean allStar) {
        isAllStar = allStar;
    }

    public Character getOrderChar() {
        return orderChar;
    }

    public void setOrderChar(Character orderChar) {
        this.orderChar = orderChar;
    }

    public String getPlayerUrl() {
        return playerUrl;
    }

    public void setPlayerUrl(String playerUrl) {
        this.playerUrl = playerUrl;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
