package ries.dan.cron;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ries.dan.service.PlayerService;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class Scheduler {

    @Autowired
    PlayerService playerService;

    private static final Logger LOGGER = LoggerFactory.getLogger(Scheduler.class);

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/YYYY HH:mm:ss");

    @PostConstruct
    public void onStartup() {
        getPlayerData();
    }

    @Scheduled(cron = "0 0 0 */1 * *")
    public void getPlayerDataScheduled() {
        getPlayerData();
    }

    public void getPlayerData() {
        LOGGER.info("Getting player data: {}", simpleDateFormat.format(new Date()));
        playerService.updatePlayerData();
        LOGGER.info("Finished updating player data: {}", simpleDateFormat.format(new Date()));
    }

}
