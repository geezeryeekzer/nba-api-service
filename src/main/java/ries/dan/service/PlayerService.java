package ries.dan.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ries.dan.model.Player;
import ries.dan.repository.PlayerRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlayerService {

    @Autowired
    private PlayerRepository playerRepository;

    public void updatePlayerData() {
        HttpComponent httpComponent = new HttpComponent();
        List<Player> playerList = httpComponent.getPlayerData();
        playerRepository.save(playerList);
    }

    public List<Player> getPlayers() {
        List<Player> playerList = new ArrayList<>();
        playerRepository.findAll().forEach(playerList::add);
        return playerList;
    }

}
