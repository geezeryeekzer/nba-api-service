package ries.dan.service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;
import ries.dan.config.Consts;
import ries.dan.model.Player;

import java.util.ArrayList;
import java.util.List;

@Component
public class HttpComponent {

    private String nbaUrl = "http://www.nba.com/";

    public List<Player> getPlayerData() {
        String jsonData = callJsonEndpoint(Consts.PLAYER_ENDPOINT);
        return convertStringToJsonToObject(jsonData);
    }

    private String callJsonEndpoint(String endpoint) {
        try(CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(nbaUrl + endpoint);
            CloseableHttpResponse response1 = httpclient.execute(httpGet);
            HttpEntity httpEntity = response1.getEntity();
            String jsonResponse = EntityUtils.toString(httpEntity);
            return jsonResponse;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private List<Player> convertStringToJsonToObject(String jsonData){
        ArrayList<Player> playerArrayList = new ArrayList<>();
        JsonParser jsonParser = new JsonParser();
        JsonElement jsonElement = jsonParser.parse(jsonData);
        JsonArray jsonArray = jsonElement.getAsJsonArray();
        for (JsonElement element : jsonArray) {
            Player player = new Gson().fromJson(element, Player.class);
            playerArrayList.add(player);
        }
        return playerArrayList;
    }

}
