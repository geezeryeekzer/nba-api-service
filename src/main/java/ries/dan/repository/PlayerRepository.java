package ries.dan.repository;

import org.springframework.data.repository.CrudRepository;
import ries.dan.model.Player;

public interface PlayerRepository extends CrudRepository<Player, String> {
}
